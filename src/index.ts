import cluster from 'cluster';
import * as os from 'os';
import { $ } from 'zx'
import { Worker } from 'bullmq';
import { Redis } from 'ioredis';

const numberOfCores = os.cpus().length;

if (cluster.isPrimary) {
  console.log(`Master ${process.pid} started`);
  for (let i = 0; i < numberOfCores; i++) {
    cluster.fork();
  }
} else {
  console.log(`Worker ${process.pid} started`);

  const connection = new Redis({
    port: 6379,
    host: "192.168.1.10",
    maxRetriesPerRequest: null,
    enableReadyCheck: false
  });

  const worker = new Worker('ants', async job => {
    const { name, data: { cmd, args }} = job
    
    console.log(`${process.pid}: ${JSON.stringify(name)}`);

    await $`${cmd} ${args}`
  }, { connection });
}
